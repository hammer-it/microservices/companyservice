const express = require('express');
const routes = require('./routes');
const cors = require('cors');
const db = require('./models')

const app = express();
const corsOptions = {
    origin: "http://localhost:8081",
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: [
        'Content-Type', 
        'Authorization', 
        'Origin', 
        'x-access-token', 
        'XSRF-TOKEN'
    ], 
    preflightContinue: false 
};

app.use(express.json());
app.use(cors(corsOptions));
app.use('/api/company', routes);

db.sequelize.sync().then((req) => {
    app.listen(process.env.PORT || '3000', () => {
        console.log(`listening on portx: ${process.env.PORT || '3000'}`)
    });
}).catch(err => {
    console.error(err)
})
