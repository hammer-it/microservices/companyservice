const express = require('express');
const router = express.Router();
const { Company, Logo, ProductSubscription } = require('../models');
const RabbitMQMessagingService = require('../rabbitMQ');

const companyService = new RabbitMQMessagingService('company');
const productsubscriptionService = new RabbitMQMessagingService('productsubscription');
const logoService = new RabbitMQMessagingService('logo');

// Get List of companies
router.get('/', async (req, res, next) => {
    Company.findAll().then( (companies) => {
        res.send(companies);
    }).catch( err => {
        console.log(err);
        throw err;
    })
});

// Get Single Company
router.get('/:id', async (req, res, next) => {
    const companyId = req.params.id;
    Company.findAll({where: {id: companyId}}).then( (company) => {
        res.status(200).send(company);
    }).catch(err => {
        console.log(err);
        throw err;
    })
});

// Create a Company
router.post('/', async (req, res, next) => {
    const company = req.body.company;
    const logo = req.body.logo;
    const productsubscription = req.body.productsubscription;

    Company.create({
        name:  company.name,
        logoPath: company.logoPath,
        location: company.location
    }).then(result => {
        if(company.logoPath !== null || company.logoPath !== ''){
            Logo.create({
                path: logo.path,
                height: logo.height,
                width: logo.width,
                company_id: result.id
            }).then(resLogo => {
                logo.id = resLogo.id,
                logoService.produce(logo);
            })
        }

        ProductSubscription.create({
            type: productsubscription.type,
            start_date: productsubscription.start_date,
            end_date:productsubscription.end_date,
            status:productsubscription.status,
            maximumUsers: productsubscription.maximumUsers,
            company_id: result.id
        }).then(resSub => {
            productsubscription.id = resSub.id;
            productsubscriptionService.produce(productsubscription);

            let msg = {
                company: company,
                logo:logo,
                productsubscription: productsubscription
            }
            res.send(msg)
        })
    })
})

// Update a specific vehicle
router.put('/id');

function checkIfDuplicate(id, type){
    let result = false;
    if(type === 'vehicle'){
        Vehicle.findAll({where: {id: id}})
        .then( () => {
            result =  true;
        })
        .catch(err => {
            console.log('Err: ' + err);
            result = false;
        })
    }else{
        Engine.findAll({where: {id: id}})
        .then( () => {
            result = true;
        })
        .catch(err => {
            console.log('Err: ' + err);
            result = false;
        });
    }

    return result;
}

module.exports = router;
