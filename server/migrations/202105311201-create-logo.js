'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Logos', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          path:{
            type: Sequelize.STRING
          },
          height:{
            type: Sequelize.INTEGER
          },
          width: {
            type: Sequelize.INTEGER
          },
          company_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Companies',
                key: 'id'
            }
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
          }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Logos');
      }
}