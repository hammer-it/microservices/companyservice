'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('ProductSubscriptions', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          type:{
            type: Sequelize.STRING
          },
          start_date:{
            type: Sequelize.DATE
          },
          end_date: {
            type: Sequelize.DATE
          },
          status:{
            type: Sequelize.BOOLEAN
          },
          maximum_users: {
            type: Sequelize.INTEGER
          },
          company_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Companies',
                key: 'id'
            }
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
          }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('ProductSubscriptions');
      }
}