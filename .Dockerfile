FROM node:latest

WORKDIR /server

COPY /package.json .

RUN npm install

COPY ./server .

EXPOSE 3000 3306 5672

CMD [ "npm", "start" ]